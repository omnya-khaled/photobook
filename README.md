![awy.jpg](https://bitbucket.org/repo/k8oEBz/images/1588400660-awy.jpg)


# What? #
appenza is hiring: 

* Talented UX Engineer to create amazing user experiences. 
* Frontend Engineer to makes the magic happens.
* Backend Engineer to be the new Zuckerberg.
* Fullstack Engineer to be our sorcerer.
* UX Mentor to be our fountain of youth.
* Technical Project Manager.

Before start please make sure that you understand the titles as we understand it through the following graph:
![flow.jpg](https://bitbucket.org/repo/k8oEBz/images/3216518561-flow.jpg)



# Where? #
We're located in 1st Settlement, Cairo, Egypt.


# Requirements #
* Agile mindset
* Know how to deal with team members and high communication skills
* Good understand of ownership principles
* Ambitious, Flexible and open minded

# How? #
To attend check the open issues and pick some to get minimum score of **100** points

1. Fork
2. Work on issues you want to.
3. Commit with a clear message with the issue number.
4. Pull Request.
5. Send email with pull request number and your resume to hr@appenza-studio.com.